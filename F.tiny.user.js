// ==UserScript==
// @name        Fantastisch!
// @namespace   https://bitbucket.org/rizenb/
// @author      doyougl0w@gmail.com
// @include     *football.fantasysports.yahoo.com*
// @icon        https://bitbucket.org/rizenb/f/raw/12b5171037e8384d77955d6ed154a3e5fa9c6b04/Ficon.png
// @require     http://code.jquery.com/jquery.min.js
// @run-at      document-start
// @resource    iconF   https://bitbucket.org/rizenb/f/raw/12b5171037e8384d77955d6ed154a3e5fa9c6b04/Ficon.png
// @resource    FantasticStyle      https://bitbucket.org/rizenb/f/raw/12b5171037e8384d77955d6ed154a3e5fa9c6b04/fantasticstyle.css
// @resource    RefreshIcon     https://bitbucket.org/rizenb/f/raw/12b5171037e8384d77955d6ed154a3e5fa9c6b04/refresh.png
// @version     1.25A
// ==/UserScript==
//#///////////////////////////////////// Notes: ////////////////////////////////////#//
//
//   Please give feedback.   doyougl0w@gmail.com
//
//#/////////////////////////////////////////////////////////////////////////////////#//
(function(window, document){
//# Don't run in frames. #//
        if (window.top !== window.self) { return };
//# Get the saved preferences or use Default #//
        var settings = JSON.parse(localStorage.getItem('FantasticSettings'));
        if (!settings || settings=='{}') {
                var settings = {
                        core: {
                                maxQs: 3,
                                years: 'this',
                                minDays: 3,
                                where: '.player',
                                muFullNames: true
                        },
                        BtnOnPage: { on: true, open: false, pages: 'all' },
                        WaitToLoad: { on: false, open: false, pages: 'all' },
                        ShowBench: { on: true, open: false, pages: ['matchup'] },
                        HideAds: { on: true, open: false, pages: ['matchup', 'leaders', 'injuries', 'playernotes', 'playerchanges', 'cantcutlist', 'statcorrections', 'playerswatch', 'dropplayer', 'proposetrade', 'teamlog', 'players', 'team' ]},
                        HideTopBar: { on: true, open: false, pages: 'all' },
                        PlayerTouches: { on: false, open: false, pages: 'all' },
                       
                };
                localStorage.setItem('FantasticSettings', JSON.stringify(settings));
                //unsafeWindow.console.log(settings);
                 /*superNotes: { on: false, open: false, pages: 'all' },
                        slimStats: { on: false, open: false, pages: 'all' },*/
                        /*fpAgainst: { on: false, open: false, pages: 'all' },
                        muRatings: { on: false, open: false, pages: 'all' },
                        weeklyTargets: { on: false, open: false, pages: 'none' },
                        watchRead: { on: false, open: false, pages: ['playerswatch']}*/
        };
//#//////////////////////////////# Append Stylesheet #////////////////////////////////#//
        var head = document.getElementsByTagName('head')[0];
        var inline_CSS = document.createElement('style');
        inline_CSS.type = 'text/css', inline_CSS.innerHTML = GM_getResourceText('FantasticStyle');
        head.appendChild(inline_CSS);
//#//////////////////////////////# Begin Functions #////////////////////////////////#//
        if (!Fantastic) {
                var Fantastic = ({
                        cache:{q:''}, reg:{}, tmp: {hasRun:false, ver: GM_info.script.version, wheres:[]},
                        subSet: {
                                def: { //# Mostly not going to be used, but here are some details leftover from planning other possibilities #//
                                        option: 'off', //# This the currently selected value, always one of the 'newPref.options'
                                        open: true, //# This is whether or not it's preference pane is currently expanded. 
                                        title: 'A sentence or two about the specific feature',  //# The description of the feature that displays on hovering over the settingswitch.
                                        notList: ['this', 'that'],  //# A list used for filtering the data for display in the Title Box on hover.
                                        afterEq: '', //# A variable for the position to insert data into a table
                                        options: { all: 'All', none: 'None', some: 'Some' }, //# These are the actual options in the preference menu for pages. //
                                        //# Default list of team abbreviations and their associated ntid's. Unfortunately still necessary for easiness. #//
                                        teams: { atl: { ntid: 1, yid: 'cin' }, buf: { ntid: 2, yid: 'buf' }, chi: { ntid: 3, yid: 'chi' }, cin: { ntid: 4, yid: 'cin' }, cle: { ntid: 5, yid: 'cle' }, dal: { ntid: 6, yid: 'dal' }, den: { ntid: 7, yid: 'den' }, det: { ntid: 8, yid: 'det' }, gb: { ntid: 9, yid: 'gb' }, ten: { ntid: 10, yid: 'ten' }, ind: { ntid: 11, yid: 'ind' }, kc: { ntid: 12, yid: 'kc' }, oak: { ntid: 13, yid: 'oak' }, stl: { ntid: 14, yid: 'stl' }, mia: { ntid: 15, yid: 'mia' }, min: { ntid: 16, yid: 'min' }, ne: { ntid: 17, yid: 'ne' }, no: { ntid: 18, yid: 'no' }, nyg: { ntid: 19, yid: 'nyg' }, nyj: { ntid: 20, yid: 'nyj' }, phi: { ntid: 21, yid: 'phi' }, ari: { ntid: 22, yid: 'ari' }, pit: { ntid: 23, yid: 'pit' }, sd: { ntid: 24, yid: 'sd' }, sf: { ntid: 25, yid: 'sf' }, sea: { ntid: 26, yid: 'sea' }, tb: { ntid: 27, yid: 'tb' }, was: { ntid: 28, yid: 'was' }, car: { ntid: 29, yid: 'car' }, jac: { ntid: 30, yid: 'jac' }, bal: { ntid: 33, yid: 'bal' }, hou: { ntid: 34, yid: 'hou' }},
                                        //# List of Default pages. #//
                                        pages: ['matchup', 'leaders', 'injuries', 'playernotes', 'playerchanges', 'cantcutlist', 'statcorrections', 'playerswatch', 'dropplayer', 'proposetrade', 'teamlog'],                                        
                                        section: 'general', //# Feature type option to allow easy splitting the features.
                                }, 
                                BtnOnPage: {
                                        title: 'Add a Button to the page that toggles the Fantastic Settings Dialog, or only access it via GM Menu Command.', open: true, afterEq: '', section: 'general', pages: 'all', 
                                        on: function() {
                                                $('#boxF').show();
                                        },
                                        off: function() {
                                                $('#boxF').hide();
                                        }
                                },
                                WaitToLoad: {
                                        title: 'Delay the loading of the Fantastic Features until one of three events is triggered.', open: true, afterEq: '', section: 'general', pages: 'all',
                                        on: function() {
                                                
                                        },
                                        off: function() {
                                                
                                        }
                                },
                                ShowBench: {
                                        title: 'Displays the bench by default on matchup pages.', open: true, afterEq: '', section: 'layout', pages: ['matchup'],
                                        on: function() {
                                                if (F.tmp.currentPage == 'matchup') {
                                                        $('#bench-toggle').text('Hide Bench Players'); //# Show Bench automatically on matchup pages #//
                                                        $('#bench-table').removeClass('Hidden'); //# Show Bench automatically on matchup pages #//
                                                }
                                        }, off: function() {
                                                if (F.tmp.currentPage == 'matchup') {
                                                        $('#bench-toggle').text('Show Bench Players'); //# Show Bench automatically on matchup pages #//
                                                        $('#bench-table').addClass('Hidden'); //# Show Bench automatically on matchup pages #//
                                                }
                                        },
                                },
                                HideAds: {
                                        title: 'This removes the Ad bar and expands the width of the the main viewport when applicable.', open: true, afterEq: '', section: 'layout', pages: 'all', 
                                        on: function() {
                                                var p = settings.HideAds.pages;
                                                if (p == 'all' || $.inArray(F.tmp.currentPage, p) !== -1) {
                                                        var y = $('#yspmain').parent().parent();
                                                        var yw = parseInt($('#fantasy').css('width').replace('px', ''))+20;
                                                        //unsafeWindow.console.log(yw);
                                                        //y.find('header:eq(0)').css('width');
                                                        if (F.tmp.currentPage=='matchup') {
                                                                y.children('section:not(#smacktalk),div').css('width', yw+'px');
                                                        }
                                                        $('[id^=yspad]').remove();
                                                }
                                        },
                                        off: function() {
                                                //$('#yspmain').css('width', '79%'); //# Stretch to fill whitespace on matchup #//
                                                //$('#yspsub').append('<div id=yspad>Donate to fantastic?</div>').fadeIn('fast') //# Hide whitespace #//
                                        }
                                },
                                HideTopBar: {
                                        title: 'This just removes the top bar.', open: true, afterEq: '', section: 'layout', pages: 'all', 
                                        on: function() {
                                                var top = $('#yucs-top-bar');
                                                if (top.length) {
                                                        top.slideUp(0);
                                                } else {
                                                        $.ready(top.slideUp(0));
                                                        unsafeWindow.console.log(top.length);
                                                }
                                        },
                                        off: function() {
                                                var top = $('#yucs-top-bar');
                                                if (top.length) {
                                                        top.slideDown(0);
                                                }
                                        }
                                },
                                PlayerTouches: {
                                        title: 'This will fetch, cache, and insert Targets and Touches information into Yahoo player tables.', open: true, afterEq: '', section: 'table', pages: 'all',
                                        on: function() {
                                                /* Test for and get Target Data and Append it into Tables
                                                 * 
                                                 * Currently Unavailable on matchup pages
                                                 */
                                                if ($.inArray(settings['PlayerTouches'].pages, F.tmp.currentPage) == -1) {
                                                        if (F.cacheNeedsUpdate('playerNames')) {
                                                                $.when(F.getFullNames(F.tmp.currentLeague)).done(function(a) {
                                                                        unsafeWindow.console.log(a);
                                                                        F.cache.playerNames = a;
                                                                        var d = JSON.stringify(a);
                                                                        //unsafeWindow.console.log(d);
                                                                        F.saveData('playerNames', d);
                                                                        F.cacheUpdated('playerNames', d.length);
                                                                })
                                                                
                                                        } else {
                                                                var names = localStorage.getItem('PlayerNames');
                                                                F.cache.playerNames = JSON.parse(names);
                                                        }
                                                        if (F.cacheNeedsUpdate('PlayerTouches')) {
                                                                var years = F.getYears();
                                                                var year = '&year_end='+years[1]+'&year_start='+years[0];
                                                                dat = "position[1]=RB&position[2]=WR&position[3]=TE&week_end=21&week_start=1"+year;
                                                                //unsafeWindow.console.log(dat);
                                                                $.when(F.get("http://www.kffl.com/fantasy-football/targets/index.php", dat)).done(function(f) {
                                                                        //unsafeWindow.console.log($(f).find('table:eq(1)').html().length);
                                                                        var oy = {};
                                                                        var o = 1;
                                                                        var poo = ["id","kid","pos","team","thrownto","receptions","rec%","utilized","carries", "source"];
                                                                        var stat = $(f).find('table:eq(1)');
                                                                        //unsafeWindow.console.log(stat);
                                                                        stat.find('tr.stats').each(function(a){
                                                                                var tds = $(this).find('td');
                                                                                var af = {};
                                                                                for (var i=0;i<=tds.length;i++) {
                                                                                        if (typeof poo[i] === 'undefined') {
                                                                                        } else if (poo[i] == 'id') {
                                                                                                af[poo[i]] = o.toString();
                                                                                        } else if (poo[i] == 'kid') {
                                                                                                var as = tds.eq(i).find('a').first();
                                                                                                var ln = 'http://kffl.com'+as.attr('href');
                                                                                                var kid = ln.match(/[0-9]+/);
                                                                                                var playername = as.text();
                                                                                                af[poo[i]] = kid.toString();
                                                                                                af['player'] = playername;
                                                                                                af['link'] = ln;
                                                                                        } else if (poo[i] == 'source') {
                                                                                                af['source'] = 'kffl.com';
                                                                                        } else if (poo[i] == 'x') {
                                                                                        } else {
                                                                                                af[poo[i]] = tds.eq(i).html();
                                                                                        }
                                                                                }
                                                                                var y = o.toString();
                                                                                oy[playername] = af;
                                                                                o++;
                                                                        });
                                                                        var yo = JSON.stringify(oy);
                                                                        F.saveData('PlayerTouches', yo);
                                                                        F.cacheUpdated('PlayerTouches', yo.length);
                                                                        F.cache.PlayerTouches = oy;
                                                                        //unsafeWindow.console.log(F.cache.PlayerTouches);
                                                                        
                                                                        F.addHeaders('PlayerTouches', 'Table:eq(0), .DataTable:eq(0)');
                                                                        F.addToTable('PlayerTouches', 'Table:eq(0), .DataTable:eq(0)');
                                                                        //F.addTargets(F.cache.PlayerTouches);
                                                                });
                                                        } else {
                                                                var tnt = localStorage.getItem('PlayerTouches');
                                                                F.cache.PlayerTouches = JSON.parse(tnt);                                                                
                                                                F.addHeaders('PlayerTouches', '.Table, .DataTable:eq(0)');
                                                                F.addToTable('PlayerTouches', '.Table, .DataTable:eq(0)');
                                                                //unsafeWindow.console.log(F.cache.PlayerTouches);
                                                                //F.addTargets(F.cache.PlayerTouches);
                                                                
                                                        }
                                                        
                                                        //$('.PlayerTouches').hover(function(e){
                                                        //        var t = $(this);
                                                        //        F.showTitle(t);
                                                        //        
                                                        //}, function(e) {
                                                        //        var t = $(this);
                                                        //        F.hideTitle(t);
                                                        //});
                                                        //F.tmp.wheres.push(F.getWhere('PlayerTouches'));
                                                } else {
                                                        unsafeWindow.console.log('Sorry, Target Info is not compatible with the matchup page yet.');
                                                }
                                        },
                                        off: function() {
                                                delete F.cache.PlayerTouches;
                                                $('th.PlayerTouches, td.PlayerTouches').remove();
                                        },
                                        tdValue: 'utilized',
                                        notList: ['player','id', 'kid', 'team', 'pos', 'link', 'href']
                                },
                        },
                        
                        
                        showTitle: function(el, e) {
                                if (typeof el !== 'object') {
                                        el = $(el);
                                }
                                var tb = $('#TitleBox'), elT = (el.offset().top)+0, elL = (el.offset().left)+(el.width()+10);
                                //unsafeWindow.console.log((el.offset().left)+(el.width()+10));
                                if (tb.size() == 0) {
                                       $('body').append('<div id="TitleBox">'+el.data("title")+'</div>');
                                       tb = $('#TitleBox')
                                       tb.css({top: elT, left: elL}).fadeIn('fast');
                                } else {
                                        tb.html(el.data("title"));
                                        tb.css({top: elT, left: elL}).fadeIn('fast');
                                }
                                //tb.css({top: elT, left: elL}).fadeIn('slow');
                                //unsafeWindow.console.log(elT + ' | '+ elL);
                                //unsafeWindow.console.log(el.data('title'));
                        },
                        hideTitle: function(el) {
                                $('#TitleBox').fadeOut(0);
                        },
                        parsePlayerDiv: function(row) {
                                var info = {};
                                var a = row.find('a.name:eq(0)');
                                //unsafeWindow.console.log(a);
                                if (a.length>0) {
                                        info['name'] = a.text();
                                        info['href'] = a.attr('href');
                                        var b = a.attr('href').split('/');
                                        info['yid'] = b.splice(-1,1)[0];
                                        unsafeWindow.console.log(info['yid']);
                                        return info;
                                } else {
                                        return info;
                                }
                        },
                        getName: function(row) {
                                if (F.tmp.currentPage !== 'matchup') {
                                        return row.find('a.name:eq(0)').text();
                                } else {
                                        return false;
                                }
                        },
                        getFullNames: function(lg) {
                                return $.Deferred(function( dfd ){
                                        var ln = 'http://football.fantasysports.yahoo.com/f1/'+lg+'/starters';
                                        var nruter = {};
                                        $.when(F.get(ln)).done(function(a) {
                                                //unsafeWindow.console.log(a)
                                                $(a).find('a.name').each(function(b) {
                                                        var c = $(this), d = c.attr('href').split('/');
                                                        e = d.splice(-1,1)[0]; //YID
                                                        //unsafeWindow.console.log(e+' | '+c.text());
                                                        nruter[e] = c.text();
                                                });
                                                //unsafeWindow.console.log(nruter);
                                                dfd.resolve(nruter);
                                        }).fail(function(a) {dfd.reject(a)});
                                }).promise();
                        },
                        fullName: function(yid) {
                                if (F.cache.playerNames) {
                                        var pn = F.cache['playerNames'];
                                        if (pn[yid]) {
                                                unsafeWindow.console.log(pn);
                                                return pn[yid];
                                        } else {
                                                return yid+' Failed';
                                        }
                                } else {
                                        return setTimeout(F.fullName, 333, yid);
                                        //return false;
                                }
                                
                        },
                        getYid: function(tr) {
                                if (tr.find('.ysf-player-name>a:eq(0)').size()>0) {
                                        var t = $(tr).find('.ysf-player-name>a:eq(0)').attr('href').split('/');
                                        return t.splice(-1,1)[0];
                                } else {
                                        //F.getYid(tr);
                                        return 0;
                                        //unsafeWindow.console.log(tr.html());
                                        //return 0;
                                }
                                
                        },
                        getYears: function() {
                                var d = new Date();
                                var dy = d.getFullYear();
                                if (settings.core.years == 'last') {
                                        return [dy-1, dy-1];
                                } else if (settings.core.years == 'both') {
                                        return [dy-1, dy];
                                } else {
                                        return [dy, dy];
                                }
                        },
                        colSpan: function(g) {
                                if (g !== F.tmp.colspan) {
                                        F.tmp.colspan = g;
                                }
                                //unsafeWindow.console.log(F.tmp.colspan);
                                $('.FTableHead').attr('colspan', g);
                        },
                        addHeaders: function(cls, dt) {
                                if (typeof dt === 'undefined') {
                                        dt = $('.Table, .DataTable');
                                } else {
                                        dt = $(dt);
                                }
                                dt.each(function() {
                                        var dts = $(this).find('thead>tr'), tro, trh;
                                        //unsafeWindow.console.log(dts);
                                        if (dts.size() > 1) {
                                                trh = $(dts[0]);
                                                if (trh.find('.FTableHead').size() == 0) {
                                                        trh.find(settings.core.where).after('<th colspan="1" class="FTableHead">Fantastic</th>');
                                                } else {
                                                        var cs = $(this).find('th.FTableHead:eq(0)').attr('colspan');
                                                        F.colSpan(parseInt(cs)+1);
                                                        //unsafeWindow.console.log(cs);
                                                }
                                                //F.addFHead(row);
                                                tro = $(dts[1]);
                                        } else {
                                                tro = $(dts[0]);
                                        }
                                        var th = tro.find('.'+cls);
                                        if (th.size() == 0) {
                                                var whe = tro.find(settings.core.where);
                                                //var dtd = $('.Table, .DataTable>tbody>tr');
                                                //unsafeWindow.console.log(whe);
                                                whe.each(function() {
                                                        $(this).after('<th class="clsFHd '+cls+'">'+cls+'</th>').next().data('title', '<span>'+F.subSet[cls].title+'</span>');
                                                });
                                        }
                                });
                        },
                        addToTable: function(cls, id) {
                                //# Do the finding of targets here. Look for multiple wheres
                                //# make each feature func accept where TD instead, return td, append here
                                //# add cls to td 
                                if (typeof id === 'undefined') {
                                        id = $('.Table, .DataTable');
                                } else {
                                        id = $(id);
                                }
                                var hs = id.find('tbody>tr');
                                hs.each(function(a) {
                                        var t = $(this);
                                        var dat = {key: 'name'};
                                        F.addToRow(cls, t, dat);
                                });
                                $('.'+cls).on('mouseenter', function(e) {
                                        //$('body').append('<div>'+$(this).data("title")+'</div>');
                                        if ($(this).hasClass('mt')) {
                                                
                                        } else {
                                                if (e.relatedTarget && e.relatedTarget.id !== 'TitleBox') {
                                                        F.showTitle($(this), e);
                                                }
                                        }
                                }).on('mouseleave', function(e) {
                                        //unsafeWindow.console.log(e.target);
                                        //unsafeWindow.console.log(e.relatedTarget);
                                        if (e.relatedTarget && e.relatedTarget.id !== 'TitleBox') {
                                                F.hideTitle($(this), e);
                                        }
                                });
                                $('table').each(function() {
                                        if ($(this).find('td.mt').size() >= $(this).find('td.'+cls).size()) {
                                                //unsafeWindow.console.log($(this).find('.mt').size());
                                                //unsafeWindow.console.log($(this).find('.'+cls).size());
                                                $(this).find('.'+cls+',.FTableHead').hide();
                                        }
                                });
                                
                        },
                        addToRow: function(cls, row, dat) {
                                //# Add another check to verify the row has a name and features added. #//
                                //# dat object must have key property #//
                                var k = (dat.key) ? dat.key : 'name';
                                var td = row.find('.'+cls);
                                var inf = F.parsePlayerDiv(row);
                                var name = inf['name'];
                                var sub = F.subSet[cls];
                                var nChk = F.cache[cls][name];
                                //unsafeWindow.console.log(F.cache);
                                //unsafeWindow.console.log(inf['yid']);
                                if (!nChk && F.tmp.currentPage == 'matchup') {
                                        var fName = F.fullName(inf['yid']);
                                        unsafeWindow.console.log(fName);
                                        var tDat = F.cache[cls][fName];
                                } else {
                                        var tDat = nChk;
                                }
                                //unsafeWindow.console.log(name+' | '+inf['name']);
                                //unsafeWindow.console.log(tDat);
                                if (tDat) {
                                        var titHead = '<span class="TitleHead">'+cls+'</span><span>Link:  <a href="'+tDat.link+'" target="_blank">'+tDat.player+'  -  '+tDat.source+'</a></span><hr />';
                                        var titLoop='', not = [];
                                        if (sub.notList) {
                                                not = sub.notList;
                                        }
                                        $.each(tDat,function(a,b) {
                                                if ($.inArray(a, not) == -1) {
                                                        titLoop = titLoop + '<span><strong>'+a+'</strong>:   '+b+'</span>'
                                                }
                                        });
                                        var tit = titHead+titLoop;
                                        if (td.size() == 1) {
                                                td = td.eq(0);
                                                td.html(tDat[sub.tdValue]).data('title', tit);
                                        } else {
                                                td = row.find(settings.core.where);
                                                td.after('<td class="clsF '+cls+'">'+tDat[sub.tdValue]+'</td>').next().data('title', tit);
                                        }
                                } else {
                                        if (td.size() == 0) {
                                                td = row.find(settings.core.where);
                                                td.after('<td class="clsF mt '+cls+'"> - </td>');
                                        }
                                }
                        },
                        url: function(uri) { //# Handle the URL and create variables appropriately #//
                                var loc = (uri) ? uri.toString().split('?') : window.location.toString().split('?');
                                if (loc.length>1) {
                                        var loq = loc[1], loc = loc[0];
                                } else {
                                        var loq = null, loc = loc[0];
                                }
                                F.tmp.location = loc;
                                if (loc.search('/f1/') !== -1) {
                                        var current = loc.split('/f1/')[1].split('/');
                                        F.tmp.currentLeague = current[0].toString();
                                        if ( current[1] && current[1].match(/\d+/) ) {
                                                F.tmp.currentTeam = current[1].toString();
                                                if (current[2]) {
                                                        F.tmp.currentPage = current[2].match(/[A-Za-z]+/).toString().toLowerCase();
                                                } else {
                                                        F.tmp.currentPage = 'team';
                                                }
                                        } else {
                                                if (current[1] && current[1].match(/[A-Za-z]+/)) {
                                                        F.tmp.currentPage = current[1].toString().toLowerCase();
                                                } else {
                                                        F.tmp.currentPage = 'league';
                                                }
                                        }
                                }
                                if (loq) { //# Handle Query String and Save Values #//
                                        var w = loq.split('&');
                                        for (var we in w) {
                                                var lo = w[we].split('=');
                                                F.tmp[lo[0]] = lo[1];
                                                if (lo[0] == 'lid') {
                                                        F.tmp.currentLeague = lo[1];
                                                        F.tmp.currentPage = 'front';
                                                }
                                        }
                                }
                                return F.tmp;
                        },
                        init: function() {
                                var t = localStorage.getItem('leagues');
                                if (!t || t == '') {
                                        $.when(F.buildCache()).done(function(a) {
                                                F.saveData('leagues', F.cache.leagues);
                                                //unsafeWindow.console.log(a);
                                                //unsafeWindow.console.log(F.cache);
                                        });
                                } else {
                                        F.cache.leagues = JSON.parse(t);
                                        //unsafeWindow.console.log(F.cache.q);
                                }
                                if (localStorage.getItem('Updates')) {
                                        F.reg = JSON.parse(localStorage.getItem('Updates'));
                                        //# Integrate with F.unLoad to assure data integrity #//
                                }
                                if (F.tmp.currentPage == 'matchup' && !F.tmp.mid1 && !F.tmp.mid2 && !F.tmp.week) {
                                        F.tmp.week = $('#yspmaincontent header').text().split(':')[0].replace('Week ', '').replace(/[^0-9]/g, '');
                                        var t2 = $('#matchup-header').find('a');
                                        F.tmp.mid1 = $(t2[0]).attr('href').split('/').pop();
                                        F.tmp.mid2 = $(t2[2]).attr('href').split('/').pop();
                                        unsafeWindow.console.log(F.tmp);
                                }
                                GM_registerMenuCommand('Fantastic Menu', F.toggleMenu);
                                GM_registerMenuCommand('localStorageKeys', F.listKeys);
                                F.modPage(F.tmp.currentPage);
                                $(window).bind('beforeunload', function(e) {
                                        F.unLoad(e);
                                        //# Build a simple Queue for active data operations to make sure no page exit corrupts data. #//
                                });
                        },
                        unLoad: function(e) {
                                //window.confirm('testing unload.');
                                unsafeWindow.console.log('Unloading. '+window.location);
                        },
                        listKeys: function() {
                                var r='';
                                for (var i=0;i<localStorage.length;i++) {
                                        r=r+localStorage.key(i)+'<br />';
                                        //unsafeWindow.console.log(localStorage.getItem(i));
                                }
                                $('#FantasticStatus').html(r);
                        },
                        pageMenu: function(id) {
                                var ret = '<div class="pageMenu" id="'+id+'-pages"><h1>'+id+'</h1><br />';
                                var def = F.subSet.def.pages, pag = settings[id].pages;
                                for (var i in def) {
                                        var ck='';
                                        if (pag == 'all') {
                                                ck = 'checked="checked"';
                                        } else if (pag == 'none') {
                                                ck = '';
                                        } else {
                                                if ($.inArray(settings[id].pages, def)) {
                                                        ck = 'checked="checked"';
                                                } else {
                                                        ck = '';
                                                }
                                        }
                                        ret = ret + '<div class="pageBox"><span class="pageMenuLabel">'+def[i]+'</span><span class="pageSwitch slideThree"><input type="checkbox" id="'+def[i]+'-Btn" '+ck+' /><label for="'+def[i]+'-Btn"></label></span></div>';
                                }
                                return ret+'</div>';
                        },
                        prefMenu: function(p) {
                                var ret = '', opts = '';
                                for (var i in F.subSet.def.options) {
                                        if (F.subSet.def.options.hasOwnProperty(i)) {
                                                opts = opts + '<option value="'+i+'">'+F.subSet.def.options[i]+'</option>';
                                        }
                                }
                                Object.keys(p).forEach(function(key) {
                                        if (key !== 'core') {
                                                var startBox = '<div id="'+key+'-Box" class="prefBox"><span class="prefLabel">'+key+'</span>';
                                                var chk = (p[key].on==true) ? 'checked="checked" value="true" ' : 'value="false" ';
                                                var switchBox = '<div class="prefSwitch slideThree"><input type="checkbox" '+chk+'id="'+key+'-Btn" /><label for="'+key+'-Btn"></label></div>';
                                                var selectBox = '<div class="prefSelect"><select class="slx" id="'+key+'-Slx'+'">'+opts+'</select></div>';
                                                var endBox = '</div>';
                                                ret = ret+startBox+switchBox+selectBox+endBox;
                                        }
                                });
                                return ret;
                        },
                        buildCache: function() { //# Generate an object containing league information. #//
                                return $.Deferred(function( dfd ){
                                        F.cache.leagues = F.parse.menu();
                                        var m = F.cache.leagues, jqs=[];
                                        dfd.resolve(m);
                                        //unsafeWindow.console.log(m);
                                        //unsafeWindow.console.log(F.tmp.currentLeague);
                                }).promise();
                        },
                        cacheNeedsUpdate: function(i, force) {
                                force = (force) ? force : false;
                                var min = settings.core.minDays;
                                var now = Date.now(), day = 1000*60*60*24;
                                var then, ida;
                                if (F.reg) {
                                        if (F.reg[i]) {
                                                then = F.reg[i].time;
                                                //unsafeWindow.console.log(then);
                                        } else {
                                                then = 0;
                                                //unsafeWindow.console.log(then);
                                        }       
                                } else {
                                        F.reg = JSON.parse(localStorage.getItem('Updates'));
                                        if (F.reg[i].time) {
                                                then = F.reg[i].time;
                                                //unsafeWindow.console.log(then);
                                        } else {
                                                then = 0;
                                                //unsafeWindow.console.log(then);
                                        }
                                }
                                
                                //unsafeWindow.console.log(then);
                                ida = localStorage.getItem(i);
                                var days = (now-then)/day;
                                if (!ida || ida == '[]' || days>min || force==true) {
                                        return true;
                                } else {
                                        return false; 
                                }
                                
                                //unsafeWindow.console.log(now-then);
                                //return (now-then < 604800) ? true : false;
                        },
                        cacheUpdated: function(i,l){
                                var now = Date.now();
                                F.reg[i] = { time: now, length: l };
                                F.saveData('Updates', JSON.stringify(F.reg));
                                //unsafeWindow.console.log(F.reg);
                        },    
                        saveData: function(i,d) { //# Stringify and Save data collections #//
                                if (typeof d == 'object') {
                                        d = JSON.stringify(d);
                                }
                                localStorage.setItem(i,d);
                                $('#FantasticStatus').append('Data Saved: '+i+'&nbsp;&nbsp;-&nbsp;&nbsp;Characters: '+d.length+'<br />' );
                        },
                        doTrue: function (v) { //# Apply specific functions when preferences of the same name are enabled #//
                                for (var nexo in v) {
                                        if (nexo !== 'core') {
                                                if (v[nexo].on !== false) {
                                                        if (typeof F.subSet[nexo].on == 'function') {
                                                                F.subSet[nexo].on();
                                                                //eval(F.subSet[nexo].on());
                                                                unsafeWindow.console.log(nexo+' Activated.');
                                                        } else {
                                                                unsafeWindow.console.log(nexo+' Attempted.');
                                                        }
                                                } else {
                                                    //unsafeWindow.console.log(nexo+' is '+v[nexo].on);
                                                }   
                                        }
                                    
                                }
                                F.tmp.hasRun = true;
                        },
                        modPage: function(currPage) {
                                if (F.tmp.hasRun !== true) {
                                        switch (currPage) {
                                                default:
                                                        F.doTrue(settings);
                                                        //unsafeWindow.console.log(currPage);
                                                break;
                                        }
                                        F.tmp.hasRun = true;    
                                }
                                
                        },
                        startMenu: function(swit) { //# Handles adding menu buttons to the yahoo Page. #//
                                $('body').append('<div id=FDialog style=display:none;></div>');
                                var FD = $('#FDialog');
                                if (settings.BtnOnPage.on == true) {
                                        $('#yucs>div:eq(1)').prepend('<div id=boxF><span id=btnFMenu section= title=Toggle Fantastic Menu.><img id=iconF width=28 height=28 src='+GM_getResourceURL("iconF")+' /></span><span id=btnFRefresh title=This will do a soft refresh of all page modifications. Useful after yahoo modifies the tables dynamically, like in player searches. It tries to do this automatically, but if it fails, hit this and it should fix.><img id=modRefresh src='+GM_getResourceURL("RefreshIcon")+' /></span></div>');
                                }
                                FD.append('<div id=Fantastic><span id=btnFClose>Close</span><div id=prefBoxes></div><div id=optBoxes></div><div id=FantasticStatus></div></div>');
                                FD.find('#prefBoxes').append(F.prefMenu(settings)).find('.slx').attr('title', 'This controls which pages the feature is active on.').each(function() {
                                        var t = $(this), i = t.val(), di = t.attr('id').replace('-Slx', ''), p = $('#'+di+'-Box');
                                        if (typeof settings[di].pages == 'Object') {
                                                this.value ='some';
                                        } else {
                                                this.value = settings[di].pages;
                                        }
                                        t.find('option').on('click', function(e) {
                                                var pd = p.data('pageBox'), opts = $('#optBoxes');
                                                if (this.value == 'some') {
                                                        if (!pd) {
                                                                p.data('pageBox', F.pageMenu(di));
                                                                pd = p.data('pageBox');
                                                        }
                                                        opts.html(pd).slideDown('fast');
                                                        $('.pageSwitch').on('click', function(e) {
                                                                e.preventDefault();
                                                                e.stopPropagation();
                                                                var t = $(this), ti = t.find('input:eq(0)'), tid = ti.attr('id').replace('-Btn', '');
                                                                var pa = t.parent().parent(), paid = pa.attr('id').replace('-pages', ''), p = settings[paid].pages;
                                                                if (ti.prop('checked') == true) {
                                                                        ti.prop('checked', false);
                                                                        ti.val('false');
                                                                        if (typeof p == 'object') {
                                                                                var ins = settings[paid].pages.indexOf(tid);
                                                                               /* unsafeWindow.console.log(ins);
                                                                                unsafeWindow.console.log(settings[paid].pages); */
                                                                                settings[paid].pages.splice(ins, 1);
                                                                        } else {
                                                                                
                                                                        }       
                                                                        if (F.tmp.currentPage == tid) {
                                                                                F.subSet[paid].off();
                                                                        }
                                                                } else {
                                                                        ti.prop('checked', true);
                                                                        ti.val('true');
                                                                        if (typeof p == 'object') {
                                                                                settings[paid].pages.push(tid);
                                                                        }
                                                                        if (F.tmp.currentPage == tid) {
                                                                                F.subSet[paid].on();
                                                                        }
                                                                }
                                                                //unsafeWindow.console.log(settings[paid]);
                                                                F.saveData('FantasticSettings', JSON.stringify(settings));
                                                        });
                                                        //settings[di].pages = F.subSet.def.pages;
                                                } else {
                                                        opts.slideUp('fast');
                                                        unsafeWindow.console.log(i);
                                                        settings[di].pages = i;
                                                        if (i == 'none') {
                                                                F.subSet[di].off();
                                                        } else {
                                                                F.subSet[di].on();
                                                        }
                                                        
                                                }
                                                unsafeWindow.console.log(settings);
                                                F.saveData('FantasticSettings', JSON.stringify(settings));
                                        });
                                });
                                $('#btnFMenu, #btnFClose').on('click', function(e) {
                                        F.toggleMenu();
                                });
                                $('#btnFRefresh').on('click', function(e) {
                                        F.tmp.hasRun = false;
                                        F.modPage(F.tmp.currentPage);
                                });
                                $('.prefSwitch').on('click', function(e) {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        var t = $(this), ti = t.find('input:eq(0)'), tid = ti.attr('id').replace('-Btn', '');
                                        if (ti.prop('checked') == true) {
                                                ti.prop('checked', false);
                                                ti.val('false');
                                                settings[tid].on = false;
                                                F.subSet[tid].off();
                                        } else {
                                                ti.prop('checked', true);
                                                ti.val('true');
                                                settings[tid].on = true;
                                                F.subSet[tid].on();
                                        }
                                        F.saveData('FantasticSettings', JSON.stringify(settings));
                                });
                                
                                //.css('width', $('#yspcontentheader').css('width'))
                        },
                        get: function(url, data) { //# Generic cross-domain request that returns raw html #//
                                return $.Deferred(function( dfd ){
                                        GM_xmlhttpRequest({
                                                method: 'POST',
                                                headers: {"Content-Type": "application/x-www-form-urlencoded"},
                                                data: (data) ? data : '&',
                                                url: url,
                                                onload: function(response) {
                                                        //unsafeWindow.console.log(response.responseText);
                                                        dfd.resolve(response.responseText);
                                                }, onerror: function(response) {
                                                        dfd.reject(response.responseText);
                                                }
                                        });
                                }).promise();
                        },
                        parse: {
                                table: function(htmlData, slx) { //# Generic table parser #//
                                        return $.Deferred(function( dfd ){
                                        //var ty = settings.types[type];
                                        ////var slx = ty.slx, sub = ty.sub;
                                                //var slx = 'table.stats';
                                                var tables = $($.parseHTML(htmlData)).find(slx), ret = [], titles = [];
                                                //unsafeWindow.console.log(tables.length);
                                                tables.each(function(){
                                                        var tbl = $(this), rows = [];
                                                        tbl.find('tr').each(function(){
                                                                var row = $(this), td = row.find('th,td');
                                                                var tn = td.map(function(a){
                                                                        return $(this).html();
                                                                }).get();
                                                                rows.push(tn);
                                                                //unsafeWindow.console.log(rows);
                                                        });
                                                        ret.push(rows);
                                                });
                                                if (ret.length == 1) {
                                                        ret=ret[0];
                                                }
                                                //unsafeWindow.console.log(ret);
                                                dfd.resolve(ret);
                                        }).promise();
                                },
                                menu: function() {
                                        var els = $('.Nav-fantasy').first().find('dt'), re = {};
                                        els.each(function() {
                                                var fi = $(this).find('a').eq(0);
                                                var se = $(this).next().find('a').eq(0);
                                                var both = fi.attr('href').replace('http://football.fantasysports.yahoo.com/f1/', '').split('/');
                                                        if (se.text().length > 0 ) {
                                                                var leagueName = se.text();
                                                        } else {
                                                                var leagueName = 'Unknown';
                                                        }
                                                re[both[0]] = {leagueId: both[0], leagueName: se.text(), teamName: fi.text(), teamId: both[1]};
                                        });
                                        return re;
                                }
                        },
                        toggleMenu: function() {
                                var FD = $('#FDialog');
                                if (FD.size()==0) {
                                        F.startMenu();
                                }
                                if (FD.is(':hidden')) {
                                        FD.fadeIn('fast');
                                } else {
                                        FD.fadeOut('fast');
                                }
                        }                        
                })
                var F = Fantastic;
        }
//# Bind Fantastic Events #//
        F.url();
        if (settings.WaitToLoad.on == true) {
                window.addEventListener('load', F.init, false);
        } else if (settings.WaitToLoad.on == false) {
                $(document).ready(function(){
                        F.init();
                        F.startMenu();
                });
        }
})(unsafeWindow, unsafeWindow.document)